1) //also try with a LinkedList - does it make any difference?

    Yes, its better for add and remove

2) list.remove(5); // what does this method do?

    It removes the item at index 5

3) list.remove(Integer.valueOf(5)); // what does this one do?

    It removes the item that has a value of 5

4)  which of the two lists performs better as the size increases?

    LinkedList for add and remove

5) what happens if you use list.remove(Integer.valueOf(77))?

    It removes the first item with the value of 77
